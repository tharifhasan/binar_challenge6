const Sequelize = require("sequelize");
const db = require("../config/db_connect");

const { DataTypes } = Sequelize;

const LoginAdmin = db.define(
  "login_admin",
  {
    username: DataTypes.STRING,
    password: DataTypes.STRING,
  },
  {
    freezeTableName: true,
  }
);

module.exports = LoginAdmin;

(async () => {
  await db.sync();
})();
