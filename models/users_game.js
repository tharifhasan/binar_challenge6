const Sequelize = require("sequelize");
const db = require("../config/db_connect");

const { DataTypes } = Sequelize;

const UserGame = db.define(
  "users_game",
  {
    username: DataTypes.STRING,
    password: DataTypes.STRING,
  },
  {
    freezeTableName: true,
  }
);

module.exports = UserGame;

(async () => {
  await db.sync();
})();
