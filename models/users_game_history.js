const Sequelize = require("sequelize");
const db = require("../config/db_connect");

const { DataTypes } = Sequelize;

const UserGameHis = db.define(
  "users_game_history",
  {
    login_time: DataTypes.DATE,
    play_time: DataTypes.STRING,
    win: DataTypes.INTEGER,
    lose: DataTypes.INTEGER,
  },
  {
    freezeTableName: true,
  }
);

module.exports = UserGameHis;

(async () => {
  await db.sync();
})();
