const Sequelize = require("sequelize");
const db = require("../config/db_connect");

const { DataTypes } = Sequelize;

const UserGameBio = db.define(
  "users_game_biodata",
  {
    address: DataTypes.STRING,
    email: DataTypes.STRING,
    phone: DataTypes.STRING,
  },
  {
    freezeTableName: true,
  }
);

module.exports = UserGameBio;

(async () => {
  await db.sync();
})();
