const Sequelize = require("sequelize");

const db = new Sequelize("db_users_game", "postgres", "postgres", {
  host: "localhost",
  dialect: "postgres",
});

module.exports = db;
