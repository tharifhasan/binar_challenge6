const playerChoose = document.querySelectorAll(".player");

playerChoose.forEach(function (e) {
  e.addEventListener("click", function () {
    const getResult = document.querySelector(".result");
    const wrapper = document.getElementById("wrapper");
    const arrow = document.querySelector(".arrow");
    if (e.classList.contains("rock") == true) {
      const playerPlay = `rock`;
      const computerPlay = computerChoose();
      const resultGame = result(playerPlay, computerPlay);
      e.style.backgroundColor = "#c4c4c4";
      if (playerPlay == computerPlay) {
        getResult.classList.add("resultDraw");
      }
      getResult.classList.add("resultPlay");
      getResult.innerHTML = resultGame;
      wrapper.classList.add("wrapperGame");
      wrapper.addEventListener("click", function () {
        alert("Jika ingin memulai permainan lagi, klik icon refresh di bawah");
      });
      const refresh = document.querySelector("img.refresh");
      refresh.style.backgroundColor = "#fff";
      refresh.style.borderRadius = "50%";
      arrow.style.color = "#c4c4c4";
    }
    if (e.classList.contains("scissors") == true) {
      const playerPlay = `scissors`;
      const computerPlay = computerChoose();
      const resultGame = result(playerPlay, computerPlay);
      e.style.backgroundColor = "#c4c4c4";
      if (playerPlay == computerPlay) {
        getResult.classList.add("resultDraw");
      }
      getResult.classList.add("resultPlay");
      getResult.innerHTML = resultGame;
      wrapper.classList.add("wrapperGame");
      wrapper.addEventListener("click", function () {
        alert("Jika ingin memulai permainan lagi, klik icon refresh di bawah");
      });
      const refresh = document.querySelector("img.refresh");
      refresh.style.backgroundColor = "#fff";
      refresh.style.borderRadius = "50%";
      arrow.style.color = "#c4c4c4";
    }
    if (e.classList.contains("paper") == true) {
      const playerPlay = `paper`;
      const computerPlay = computerChoose();
      const resultGame = result(playerPlay, computerPlay);
      e.style.backgroundColor = "#c4c4c4";
      if (playerPlay == computerPlay) {
        getResult.classList.add("resultDraw");
      }
      getResult.classList.add("resultPlay");
      getResult.innerHTML = resultGame;
      wrapper.classList.add("wrapperGame");
      wrapper.addEventListener("click", function () {
        alert("Jika ingin memulai permainan lagi, klik icon refresh di bawah");
      });
      const refresh = document.querySelector("img.refresh");
      refresh.style.backgroundColor = "#fff";
      refresh.style.borderRadius = "50%";
      arrow.style.color = "#c4c4c4";
    }
  });
});

// pilihan computer
const computerChoose = function () {
  const comChoose = Math.floor(Math.random() * 3) + 1;
  const rock = document.querySelector(".comRock");
  const scissors = document.querySelector(".comScissors");
  const paper = document.querySelector(".comPaper");

  if (comChoose == 1) {
    rock.style.backgroundColor = "#c4c4c4";
    return `rock`;
  }
  if (comChoose == 2) {
    scissors.style.backgroundColor = "#c4c4c4";
    return `scissors`;
  }
  if (comChoose == 3) {
    paper.style.backgroundColor = "#c4c4c4";
    return `paper`;
  }
};

// membuat aturan permainan
function result(player, comp) {
  if (player == comp) return `DRAW`;
  if (player == `rock`) return comp == `scissors` ? `PLAYER 1 WIN` : `COM WIN`;
  if (player == `scissors`) return comp == `rock` ? `COM WIN` : `PLAYER 1 WIN`;
  if (player == `paper`) return comp == `scissors` ? `COM WIN` : `PLAYER 1 WIN`;
}
