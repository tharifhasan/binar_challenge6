const express = require("express");
const app = express();
const port = 3000;
const UserRoute = require("./routes/UserRoute.js");

app.use(
  express.urlencoded({
    extended: false,
  })
);
app.use(express.json());
app.use(express.static("public"));
app.set("view engine", "ejs");
app.use(UserRoute);

app.listen(port, (req, res, err) => {
  if (err == true) {
    res.send(err);
  } else {
    console.log("Success!!!");
  }
  console.log(`Listening on Port ${port}`);
});
