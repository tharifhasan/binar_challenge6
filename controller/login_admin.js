const LoginAdmin = require("../models/login_admin.js");

const GetAdmin = async (req, res) => {
  res.render("login");
  try {
    var userAdmin = req.body.username;
    var userPassword = req.body.password;
    const Admin = LoginAdmin.findOne({
      where: {
        username: userAdmin,
        password: userPassword,
      },
    });
    if (!Admin) {
      res.status(402).json({
        succes: false,
      });
    } else {
      res.status(200).json({
        success: true,
      });
    }
  } catch (err) {
    console.log(err);
  }
};

module.exports = { GetAdmin };
