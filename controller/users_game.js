const UserGame = require("../models/users_game.js");

const GetUsers = async (req, res) => {
  try {
    const UserData = await UserGame.findAll();
    res.render("./dashboard/dashboard", { UserData });
  } catch (err) {
    console.log(err.message);
  }
};

const GetUsersById = async (req, res) => {
  try {
    const UserData = await UserGame.findOne({
      where: { id: req.params.id },
    });
    res.render("./dashboard/update", { UserData });
  } catch (err) {
    console.log(err.message);
  }
};

const CreateUsers = async (req, res) => {
  try {
    const UserData = await UserGame.create({
      username: req.body.username,
      password: req.body.password,
    }).then((UserData) => {
      res.redirect("/create_bio");
    });
  } catch (err) {
    console.log(err.message);
  }
};

const updateUsers = async (req, res) => {
  try {
    const UserData = await UserGame.update(
      {
        username: req.body.username,
        password: req.body.password,
      },
      {
        where: { id: req.params.id },
      }
    );
    res.status(201).json({ msg: UserData });
  } catch (err) {
    console.log(err);
  }
};

const DeleteUsers = async (req, res) => {
  try {
    const UserData = await UserGame.destroy({
      where: { id: req.params.id },
    });
    res.redirect("/dashboard");
  } catch (err) {
    console.log(err);
  }
};

module.exports = {
  GetUsers,
  CreateUsers,
  updateUsers,
  GetUsersById,
  DeleteUsers,
};
