const UserGameHis = require("../models/users_game_history.js");

const GetUsersHis = async (req, res) => {
  try {
    const UserDataHis = await UserGameHis.findAll();
    res.render("./dashboard/dashboard_history", { UserDataHis });
  } catch (err) {
    console.log(err.message);
  }
};

const GetUsersByIdHis = async (req, res) => {
  try {
    const UserDataHis = await UserGameHis.findOne({
      where: { id: req.params.id },
    });
    res.render("./dashboard/dashboard_history_user", { UserDataHis });
  } catch (err) {
    console.log(err.message);
  }
};

const CreateUsersHis = async (req, res) => {
  try {
    const UserDataHis = await UserGameHis.create({
      login_time: req.body.logintime,
    }).then((UserDataHis) => {
      res.redirect("/dashboard");
    });
  } catch (err) {
    console.log(err);
  }
};

module.exports = { GetUsersHis, CreateUsersHis, GetUsersByIdHis };
