const UserGameBiodata = require("../models/users_game_biodata.js");

const GetUsersBio = async (req, res) => {
  try {
    const UserDataBio = await UserGameBiodata.findAll();
    res.render("./dashboard/dashboard_biodata", { UserDataBio });
  } catch (err) {
    console.log(err.message);
  }
};

const GetUsersByIdBio = async (req, res) => {
  try {
    const UserDataBio = await UserGameBiodata.findOne({
      where: { id: req.params.id },
    });
    res.render("./dashboard/dashboard_biodata_user", { UserDataBio });
  } catch (err) {
    console.log(err.message);
  }
};

const CreateUsersBio = async (req, res) => {
  try {
    const UserDataBio = await UserGameBiodata.create({
      address: req.body.address,
      email: req.body.email,
      phone: req.body.phone,
    }).then((UserDataBio) => {
      res.redirect("/create_his");
    });
  } catch (err) {
    console.log(err);
  }
};

const updateUsersBio = async (req, res) => {
  try {
    const UserData = await UserGameBiodata.update(
      {
        address: req.body.address,
        email: req.body.email,
        phone: req.body.phone,
      },
      {
        where: { id: req.params.id },
      }
    );
    res.redirect("/user_biodata");
  } catch (err) {
    console.log(err);
  }
};

const DeleteUsersBio = async (req, res) => {
  try {
    const UserData = await UserGameBiodata.destroy({
      where: { id: req.params.id },
    });
    res.redirect("/dashboard");
  } catch (err) {
    console.log(err);
  }
};

module.exports = {
  GetUsersByIdBio,
  CreateUsersBio,
  GetUsersBio,
  updateUsersBio,
  DeleteUsersBio,
};
