const { render } = require("ejs");
const express = require("express");
const {
  GetUsers,
  CreateUsers,
  updateUsers,
  GetUsersById,
  DeleteUsers,
} = require("../controller/users_game.js");
const {
  GetUsersByIdBio,
  CreateUsersBio,
  GetUsersBio,
  updateUsersBio,
} = require("../controller/users_game_biodata.js");
const {
  GetUsersHis,
  CreateUsersHis,
  GetUsersByIdHis,
} = require("../controller/users_game_history");

const { GetAdmin } = require("../controller/login_admin.js");

const admin = {
  username: "admin",
  password: "admin",
};

const router = express.Router();

router.get("/", (req, res) => {
  res.render("index");
});

router.get("/gameplay", (req, res) => {
  res.render("game");
});

router.get("/dashboard", GetUsers);

router.get("/create", (req, res) => {
  res.render("./dashboard/create_users");
});

router.post("/create", CreateUsers);

router.get("/update/:id", GetUsersById);

router.post("/update/:id", updateUsers);

router.get("/delete/:id", DeleteUsers);
router.get("/user_biodata", GetUsersBio);
router.get("/dashboard/biodata/:id", GetUsersByIdBio);

router.get("/create_bio", (req, res) => {
  res.render("./dashboard/create_users_biodata");
});

router.post("/create_bio", CreateUsersBio);

router.get("/update_bio/:id", (req, res) => {
  res.render("./dashboard/updateBio");
});

router.post("/update_bio/:id", updateUsersBio);

router.get("/create_his", (req, res) => {
  res.render("./dashboard/create_users_history");
});

router.post("/create_his", CreateUsersHis);
router.get("/dashboard_his", GetUsersHis);
router.get("/dashboard_his/:id", GetUsersByIdHis);

router.get("/login", (req, res) => {
  res.render("login");
});

router.post("/login", (req, res) => {
  const username = req.body.username;
  const password = req.body.password;
  if (admin.username == username && admin.password == password) {
    res.redirect("/dashboard");
  } else {
    res.status(404).json({
      msg: `Cannot find data login username : ${username}, password : ${password}`,
    });
  }
});

module.exports = router;
